# React kickstart image

`make build` to download the basic react dependencies

`make start` to start the development server

Once the development server is running you should be prompted a message like:

> You can now view app in the browser.
>
> Local:            http://localhost:3000/
>
> On Your Network:  http://172.31.0.2:3000/
>
> Note that the development build is not optimized.
> To create a production build, use yarn build.

You can access the application using the second URL provided.

From now on every change you make will fire a reload of the application in the browser.

