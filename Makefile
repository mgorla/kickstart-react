scaffold:
	docker-compose up -d
	docker-compose run --rm -u root app npm install -g create-react app
	docker-compose run --rm -u root app sh -c "cd /home/node && npx create-react-app app"

build:
	docker-compose run --rm -u root app npm install

start:
	docker-compose run --rm -u root app npm start

